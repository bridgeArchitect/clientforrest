﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;

namespace RestGoClient
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public struct Answer
        {
            public string value;
        }

        private void CheckButton_Click(object sender, EventArgs e)
        {

            /* create query */
            var baseAddress = string.Format("http://192.168.89.128:8000//isLoginFree//{0}", loginBox.Text);
            var http = (HttpWebRequest)WebRequest.Create(new Uri(baseAddress));
            http.Accept = "application/json";
            http.ContentType = "application/json";
            http.Method = "GET";

            /* receive response */
            var response = http.GetResponse();
            var stream = response.GetResponseStream();
            var sr = new StreamReader(stream);
            var content = sr.ReadToEnd();
            string answer = content.ToString();

            /* deserialize json-object and write answer */
            JavaScriptSerializer js = new JavaScriptSerializer();
            Answer answerObject = js.Deserialize<Answer>(answer);
            answerBox.Text = answerObject.value;

        }

        private void AddButton_Click(object sender, EventArgs e)
        {

            /* create query */
            var baseAddress = string.Format("http://192.168.89.128:8000//addLogin//{0}", loginBox.Text);
            var http = (HttpWebRequest)WebRequest.Create(new Uri(baseAddress));
            http.Accept = "application/json";
            http.ContentType = "application/json";
            http.Method = "GET";

            /* receive response */
            var response = http.GetResponse();
            var stream = response.GetResponseStream();
            var sr = new StreamReader(stream);
            var content = sr.ReadToEnd();
            string answer = content.ToString();

            /* deserialize json-object and write answer */
            JavaScriptSerializer js = new JavaScriptSerializer();
            Answer answerObject = js.Deserialize<Answer>(answer);
            answerBox.Text = answerObject.value;

        }
    }
}
