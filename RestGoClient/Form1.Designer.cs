﻿namespace RestGoClient
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkButton = new System.Windows.Forms.Button();
            this.addButton = new System.Windows.Forms.Button();
            this.loginBox = new System.Windows.Forms.TextBox();
            this.answerBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // checkButton
            // 
            this.checkButton.Location = new System.Drawing.Point(12, 12);
            this.checkButton.Name = "checkButton";
            this.checkButton.Size = new System.Drawing.Size(211, 56);
            this.checkButton.TabIndex = 0;
            this.checkButton.Text = "Check user";
            this.checkButton.UseVisualStyleBackColor = true;
            this.checkButton.Click += new System.EventHandler(this.CheckButton_Click);
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(242, 12);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(225, 56);
            this.addButton.TabIndex = 1;
            this.addButton.Text = "Add user";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // loginBox
            // 
            this.loginBox.Location = new System.Drawing.Point(12, 74);
            this.loginBox.Name = "loginBox";
            this.loginBox.Size = new System.Drawing.Size(455, 22);
            this.loginBox.TabIndex = 2;
            // 
            // answerBox
            // 
            this.answerBox.Enabled = false;
            this.answerBox.Location = new System.Drawing.Point(12, 102);
            this.answerBox.Multiline = true;
            this.answerBox.Name = "answerBox";
            this.answerBox.Size = new System.Drawing.Size(455, 146);
            this.answerBox.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(487, 270);
            this.Controls.Add(this.answerBox);
            this.Controls.Add(this.loginBox);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.checkButton);
            this.Name = "Form1";
            this.Text = "Main Form";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button checkButton;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.TextBox loginBox;
        private System.Windows.Forms.TextBox answerBox;
    }
}

